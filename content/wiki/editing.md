+++
title = "Editing"
weight = 2

[taxonomies]
authors = ["Albatros",]
tags = ["about",]

+++

# Editing

On each leaf page, there is a `.md` link to the Git repo if you want to edit the file. You can configure the link in `config.extra.content_source`.
+++
title = "Albatros"
description = "Developer, дос and Khazak Pride"

[extra]
full_name = "Dauren Bizhanov"
avatar = "Dauren.png"
website = "https://github.com/brauden"
phone = ""
email = "https://www.linkedin.com/in/dauren-bizhanov/"
xmpp = ""

[taxonomies]
authors = ["Albatros"]
+++

Dauren Bizhanov, a man who chose to assemble his life in C. 

Uncle by force, fisherman by vice.
+++
title = "TEMPLATE" # string identical to file name and author name
description = "Serving First-Gen Graduate Students"

[extra]
full_name = "Duke F1rsts" # complete name
avatar = "dukef1rsts.png" # image in /static/team
website = "https://sites.duke.edu/dukef1rsts/"
phone = "+33 6 12 34 56 78"
email = "mail@domaine.tld"
xmpp = "pseudo@domaine.tld"

[taxonomies]
authors = ["TEMPLATE",] # everyone is author from it's own page
+++

Et qui temporibus voluptas voluptatum. Sit et qui est. Est ex repudiandae est.
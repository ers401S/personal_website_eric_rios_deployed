# Personal Website Eric Rios


## My Personal Website Repository

This is my personal Website!!!

[Demo Video showing how to make your own personal website!!!](https://youtu.be/H4AQ1bYTpLg)

Link : https://personal-website-eric-rios-deployed-ers401s-96805678e4f2fbcdfc8.gitlab.io/

![Alt text](image.png)

## Description
This project uses a tool called Zola to build a static website. My used theme is albatros.

## Installation
Here is the [zola website](https://www.getzola.org/).

Use the documentation there to build zola and serve a website, per their terminology. Edit the html pages to your liking, a little bit at a time. In a short while, you'll find that you built something cool. 

```
zola init your_website
# Download a theme from the website
# Copy the theme's files to the project
# Edit to your heart's content.
zola serve # to see your website run
# open the local link in your local browser
```

## Authors and acknowledgment
Thanks [Hugo](https://git.42l.fr/HugoTrentesaux/Albatros) for making the albatros template.

## Project status
Updated last February 2024.
